#ifndef GPU_H
#define GPU_H

typedef struct GPU
{
	reg8* pixels;
	int scanlineCounter;
	int LCDEnableCounter;

	reg8* GPUcontrol;
	reg8* scy, * scx;
	reg8* winy, * winx;
	reg8* line;
	reg8* sprPalette1;
	reg8* sprPalette2;
} GPU;

#endif