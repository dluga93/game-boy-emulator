int getColor(reg8 colorId, unsigned short pal)
{
	int col = (pal & (1 << (colorId*2+1))) ? 1 : 0;	// high bit
	col <<= 1;
	col |= (pal & (1 << (colorId*2))) ? 1 : 0;		// low bit

	return col;
}

void renderTiles()
{
	// needs optimizing
	unsigned short tileMap = 0;
	unsigned short tileSet = 0;
	bool unsig = true;

	bool usingWindow = false;

	// is the window enabled?
	if ((*cpu.gpu.GPUcontrol) & (1<<5))
	{
		// is the current scanline inside the window
		// should I check for bottom limit? scanline < winy?
		if (SCANLINE >= WINY)
			usingWindow = true;
	}

	// which tileset we are using
	if ((*cpu.gpu.GPUcontrol) & (1<<4))
		tileSet = 0;
	else
	{
		// use signed byte as tile ID
		tileSet = 0x800;
		unsig = false;
	}

	// which tilemap
	if (!usingWindow)
	{
		if ((*cpu.gpu.GPUcontrol) & (1<<3))
			tileMap = 0x1C00;
		else
			tileMap = 0x1800;
	}
	else
	{
		if ((*cpu.gpu.GPUcontrol) & (1<<6))
			tileMap = 0x1C00;
		else
			tileMap = 0x1800;
	}

	reg8 yPos;

	if (!usingWindow)
		yPos = SCY + SCANLINE;	// absolute
	else
		yPos = SCANLINE - WINY;	// relative to window // check here

	// which tile row in the tilemap are we in
	unsigned short tileYOff = ((reg8)(yPos/8)) * 32;

	for (int px = 0; px < 160; ++px)
	{
		reg8 xPos = SCX + px;

		// translate xpos to window space
		if (usingWindow)
			if (px >= WINX)	// check for other bound?
				xPos = px - WINX;

		// which of the 32 horizontal tiles does xPos fall in
		unsigned char tileXOff = xPos / 8;
		short tileNum;

		// get the tile ID
		unsigned short tileAddr = tileMap + tileYOff + tileXOff;

		if (unsig)
			tileNum = (reg8)cpu.mem.VRAM[tileAddr];
		else
			tileNum = (char)cpu.mem.VRAM[tileAddr];

		// get tile location in tileset
		unsigned short tileLocation = tileSet;

		if (unsig)
			tileLocation += (tileNum * 16);
		else
			tileLocation += ((tileNum+128) * 16);

		// which pixel line in the tile we're on
		reg8 line = yPos % 8;
		line *= 2;	// two bytes of memory
		reg8 data1 = cpu.mem.VRAM[tileLocation + line];
		reg8 data2 = cpu.mem.VRAM[tileLocation + line + 1];

		// position of the bit for the current pixel
		int colorBit = 7 - (xPos % 8);

		// get the color ID
		reg8 colorId = (data2 & (1 << colorBit)) ? 1 : 0;
		colorId <<= 1;
		colorId |= (data1 & (1 << colorBit)) ? 1 : 0;

		int col = getColor(colorId, BG_PALETTE);
		int pxIndex = SCANLINE * 160 + px;
		cpu.gpu.pixels[pxIndex] = col;
		/*switch (col)
		{
			case 0: cpu.gpu.pixels[pxIndex] = 0xFF; break;
			case 1: cpu.gpu.pixels[pxIndex] = 0xCC; break;
			case 2: cpu.gpu.pixels[pxIndex] = 0x77; break;
			case 3: cpu.gpu.pixels[pxIndex] = 0x00; break;
		}*/
	}
}

void renderSprites()
{
	bool use8x16 = false;
	if ((*cpu.gpu.GPUcontrol) & (1<<2))
		use8x16 = true;

	for (int spr = 0; spr < 40; ++spr)
	{
		// sprite occupies 4 bytes in the attribute table
		reg8 index = spr*4;
		reg8 yPos = cpu.mem.SpriteInfo[index] - 16;
		reg8 xPos = cpu.mem.SpriteInfo[index+1] - 8;
		reg8 location = cpu.mem.SpriteInfo[index+2];
		reg8 attributes = cpu.mem.SpriteInfo[index+3];

		bool yFlip = attributes & (1<<6);
		bool xFlip = attributes & (1<<5);

		int ysize = 8;
		if (use8x16)
		{
			ysize = 16;
			location &= 0xFE; // lowest significant bit is ignored in 8x16 mode
		}

		// does this sprite intercept with the current scanline
		if ((SCANLINE >= yPos) && (SCANLINE < (yPos+ysize)))
		{
			int line = SCANLINE - yPos;

			if (yFlip)
				line = ysize - line - 1;

			line <<= 1;	// *= 2
			unsigned short dataAddress = location*16 + line;
			reg8 data1 = cpu.mem.VRAM[dataAddress];
			reg8 data2 = cpu.mem.VRAM[dataAddress+1];

			for (int tilePixel = 7; tilePixel >= 0; tilePixel--)
			{
				int colorBit = tilePixel;

				if (xFlip)
					colorBit = 7 - colorBit;

				reg8 colorId = (data2 & (1 << colorBit)) ? 1 : 0;
				colorId <<= 1;
				colorId |= (data1 & (1 << colorBit)) ? 1 : 0;

				int col = getColor(colorId, attributes&(1<<4)?SPR_PAL2:SPR_PAL1);
				int pxIndex = SCANLINE*160 + xPos + (7-tilePixel);

				if (attributes & (1<<7))
					if (cpu.gpu.pixels[pxIndex] != 0)
						continue;

				if (colorId != 0)
					cpu.gpu.pixels[pxIndex] = col;
			}
		}
	}
}

void renderscan()
{
	reg8 control = (*cpu.gpu.GPUcontrol);
	if (LCDEnabled())
	{
		if (control & (1<<0))
			renderTiles();

		if (control & (1<<1))
			renderSprites();
	}
}

void SetLCDStatus()
{
	reg8 status = cpu.mem.arr[LCD_STATUS];
	if (!LCDEnabled())
	{
		// set mode to 1 during lcd disabled and reset scanline
		cpu.gpu.scanlineCounter = 0;
		SCANLINE = 0;
		status &= 0x7C;	// turn off mode bits and coincidence bit
		SetBit(status, 0);	// set mode to 1
		ClearBit(status, 1); // set second bit to 0, so the mode doesn't become 3 if second bit is already 1
		cpu.mem.arr[LCD_STATUS] = status;
		return;
	}
	else if (cpu.gpu.LCDEnableCounter > 0)
	{
		return;
	}

	reg8 currentMode = status & 0x3;
	reg8 mode = 0;
	bool reqInt = false;

	// if in vblank, set mode to 1
	if (SCANLINE >= 144)
	{
		mode = 1;
		SetBit(status, 0);
		ClearBit(status, 1);
		reqInt = status & (1<<4);
	}
	else
	{
		int mode2bounds = 80;
		int mode3bounds = mode2bounds + 172;

		// mode 2
		if (cpu.gpu.scanlineCounter < mode2bounds)
		{
			mode = 2;
			SetBit(status, 1);
			ClearBit(status, 0);
			reqInt = status & (1<<5);
		}
		// mode 3
		else if (cpu.gpu.scanlineCounter < mode3bounds)
		{
			mode = 3;
			SetBit(status, 1);
			SetBit(status, 0);
		}
		// mode 0
		else
		{
			mode = 0;
			ClearBit(status, 0);
			ClearBit(status, 1);
			reqInt = status & (1<<3);
		}
	}

	// entered new mode. request interrupt
	if (reqInt && (mode != currentMode))
		RequestInterrupt(1);

	// check coincidence flag
	if (SCANLINE == cpu.mem.arr[0xFF44+1])
	{
		SetBit(status, 2);
		if (status & (1<<6))
			RequestInterrupt(1);
	}
	else
	{
		ClearBit(status, 2);
	}

	cpu.mem.arr[LCD_STATUS] = status;
}

void step()
{
	/**
	* TODO: this method changes the LCD mode. That can depend on the scanlineCounter.
	* So we might need to update the counter with the last operation's clock cycles
	* before we call this method?
	*/
	SetLCDStatus();

	if (LCDEnabled())
	{
		if (cpu.gpu.LCDEnableCounter <= 0)
		{
			cpu.gpu.LCDEnableCounter = 0;
			cpu.gpu.scanlineCounter += cpu.t;
		}
		else
			cpu.gpu.LCDEnableCounter -= cpu.t;
	}
	else
	{
		return;
	}

	// if hblank mode just ended
	if (cpu.gpu.scanlineCounter >= 456)
	{
		// move to next scanline
		SCANLINE++;

		cpu.gpu.scanlineCounter -= 456;

		// check whether we have entered VBlank
		if (SCANLINE == 144)
		{
			RequestInterrupt(0);
			render();
		}

		// if gone past 153 (+10 lines for vblank) reset to 0
		else if (SCANLINE > 153)
		{
			SCANLINE = 0;
		}

		if (SCANLINE < 144)
			renderscan();
	}
}