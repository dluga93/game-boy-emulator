void execNext()
{
	if (cpu.halted)
	{
		cpu.t += 4;
		return;
	}

	if (cpu.haltBug)
	{
		// save the current value in the pc position in memory
		// and the two subsequent values in case of longer ops
		unsigned char tmp[3];
		memcpy(&(tmp[0]), &(cpu.mem.arr[cpu.pc]), 3);

		// save the current pc
		unsigned short tmpPc = cpu.pc;
		
		// push those 3 bytes in memory to the right, not changing the first one
		// e.g. 0x315524 --> 0x313155
		// TODO: should it be 0x315524 --> 0x31315524?
		cpu.mem.arr[cpu.pc+2] = cpu.mem.arr[cpu.pc+1];
		cpu.mem.arr[cpu.pc+1] = cpu.mem.arr[cpu.pc];

		// execute next op with new memory layout
		// and halt bug turned off
		cpu.haltBug = false;
		if (cpu.IMEOnReq)
		{
			execNext();
			cpu.IMEOnReq = false;
			cpu.masterInterrupt = true;
		}
		else if (cpu.IMEOffReq)
		{
			execNext();
			cpu.IMEOffReq = false;
			cpu.masterInterrupt = false;
		}
		else
			execNext();

		// restore the saved memory
		memcpy(&(cpu.mem.arr[tmpPc]), &(tmp[0]), 3);

		// restore the pc to the correct position
		cpu.pc--;

		return;
	}

	unsigned int op = cpu.mem.arr[cpu.pc++];

	switch (op)
	{
		case 0:		// NOP
		{
			cpu.t = 4;
			break;
		}
		case 1:		// LD BC, nn
		{
			cpu.c = cpu.mem.arr[cpu.pc++];
			cpu.b = cpu.mem.arr[cpu.pc++];
			cpu.t = 12;
			break;
		}
		case 2:		// LD (BC), A
		{
			writeMemory(cpu.bc, cpu.a);
			cpu.t = 8;
			break;
		}
		case 3:		// INC BC
		{
			cpu.bc++;
			cpu.t = 8;
			break;
		}
		case 4:		// INC B
		{
			cpu.b++;
			cpu.f = (cpu.f & 0x7F) |
							((cpu.b == 0) << 7);	// zero flag
			cpu.f &= 0xBF;	// subtract flag
			cpu.f = (cpu.f & 0xDF) |
							((!(cpu.b & 0xF)) << 5); // halfcarry
			cpu.t = 4;
			break;
		}
		case 5:		// DEC B
		{
			cpu.b--;
			cpu.f = (cpu.f & 0x7F) |
						zeroTable[cpu.b];	// zero flag
			cpu.f |= 0x40;	// subtract flag
			cpu.f = (cpu.f & 0xDF) |
						(((cpu.b & 0xF) == 0xF) << 5); // halfcarry
			cpu.t = 4;
			break;
		}
		case 6:		// LD B, n
		{
			cpu.b = cpu.mem.arr[cpu.pc++];
			cpu.t = 8;
			break;
		}
		case 7:		// RLC A
		{
			cpu.f = (cpu.a & 0x80) >> 3; // carry
			cpu.a = (cpu.a << 1) | (cpu.a >> 7);
			cpu.t = 4;
			break;
		}
		case 8:		// LD (nn), SP
		{
			unsigned int addr = cpu.mem.arr[cpu.pc+1];
			addr <<= 8;
			addr |= cpu.mem.arr[cpu.pc];
			cpu.pc += 2;
			writeMemory(addr, cpu.sp & 0xFF);
			writeMemory(addr+1, cpu.sp >> 8);
			cpu.t = 20;
			break;
		}
		case 9:		// ADD HL, BC
		{
			unsigned int sum = cpu.hl + cpu.bc;
			cpu.f = (cpu.f & 0xDF) |
						(((cpu.hl & 0xFFF) > (sum & 0xFFF))
						<< 5);	// halfcarry
			cpu.hl = sum;

			cpu.f = (cpu.f & 0xEF) |
							((sum > 0xFFFF) << 4);	// carry
			cpu.f &= 0xBF;	// subtract flag
			cpu.t = 8;
			break;
		}
		case 0xA:	// LD A, (BC)
		{
			cpu.a = cpu.mem.arr[cpu.bc];
			cpu.t = 8;
			break;
		}
		case 0xB:	// DEC BC
		{
			cpu.bc--;
			cpu.t = 8;
			break;
		}
		case 0xC:	// INC C
		{
			cpu.c++;
			cpu.f = (cpu.f & 0x3F) |
							(zeroTable[cpu.c]);	// zero and subtract flag
			cpu.f = (cpu.f & 0xDF) | ((!(cpu.c & 0xF)) << 5); //halfcarry
			cpu.t = 4;
			break;
		}
		case 0xD:	// DEC C
		{
			cpu.c--;
			cpu.f = (cpu.f & 0x7F) | (zeroTable[cpu.c]);	// zero flag
			cpu.f |= 0x40;	// subtract flag
			cpu.f = (cpu.f & 0xDF) | (((cpu.c&0xF)==0xF)<<5);//halfcarry
			cpu.t = 4;
			break;
		}
		case 0xE:	// LD C, n
		{
			cpu.c = cpu.mem.arr[cpu.pc++];
			cpu.t = 8;
			break;
		}
		case 0xF:	// RRC A
		{
			cpu.f = (cpu.a & 0x01) << 4; // carry
			cpu.a = (cpu.a >> 1) | (cpu.a << 7);
			cpu.t = 4;
			break;
		}
		case 0x10:	// STOP
		{
			// tbc
			cpu.t = 4;
			break;
		}
		case 0x11:	// LD DE, nn
		{
			cpu.e = cpu.mem.arr[cpu.pc++];
			cpu.d = cpu.mem.arr[cpu.pc++];
			cpu.t = 12;
			break;
		}
		case 0x12:	// LD (DE), A
		{
			writeMemory(cpu.de, cpu.a);
			cpu.t = 8;
			break;
		}
		case 0x13:	// INC DE
		{
			cpu.de++;
			cpu.t = 8;
			break;
		}
		case 0x14:	// INC D
		{
			cpu.d++;
			cpu.f = (cpu.f & 0x3F) |
							(zeroTable[cpu.d]);	// zero and sub flag
			cpu.f = (cpu.f & 0xDF) | ((!(cpu.d & 0xF)) << 5); //halfcarry
			cpu.t = 4;
			break;
		}
		case 0x15:	// DEC D
		{
			cpu.d--;
			cpu.f = (cpu.f & 0x7F) | (zeroTable[cpu.d]);	// zero flag
			cpu.f |= 0x40;	// subtract flag
			cpu.f = (cpu.f & 0xDF) | (((cpu.d&0xF)==0xF)<<5);//halfcarry
			cpu.t = 4;
			break;
		}
		case 0x16:	// LD D, n
		{
			cpu.d = cpu.mem.arr[cpu.pc++];
			cpu.t = 8;
			break;
		}
		case 0x17:	// RL A
		{
			unsigned int carry = (cpu.f & 0x10);
			cpu.f = (cpu.a & 0x80) >> 3;
			cpu.a = (cpu.a << 1) | (carry >> 4);
			cpu.t = 4;
			break;
		}
		case 0x18:	// JR n
		{
			cpu.pc += ((char)(cpu.mem.arr[cpu.pc]))+1;
			cpu.t = 12;
			break;
		}
		case 0x19:	// ADD HL, DE
		{
			unsigned int sum = cpu.hl + cpu.de;
			cpu.f = (cpu.f & 0xDF) |
						(((cpu.hl & 0xFFF) > (sum & 0xFFF))
						<< 5);	// halfcarry
			cpu.hl = sum;
			cpu.f = (cpu.f & 0xEF) |
							((sum > 0xFFFF) << 4);	// carry
			cpu.f &= 0xBF;	// subtract flag
			cpu.t = 8;
			break;
		}
		case 0x1A:	// LD A, (DE)
		{
			cpu.a = cpu.mem.arr[cpu.de];
			cpu.t = 8;
			break;
		}
		case 0x1B:	// DEC DE
		{
			cpu.de--;
			cpu.t = 8;
			break;
		}
		case 0x1C:	// INC E
		{
			cpu.e++;
			cpu.f = (cpu.f & 0x3F) | zeroTable[cpu.e];	// zero and sub flag
			cpu.f = (cpu.f & 0xDF) | ((cpu.e&0xF)?0:0x20); // half flag
			cpu.t = 4;
			break;
		}
		case 0x1D:	// DEC E
		{
			cpu.e--;
			cpu.f = (cpu.f & 0x7F) | zeroTable[cpu.e];	// zero flag
			cpu.f |= 0x40;	// subtract flag
			cpu.f = (cpu.f & 0xDF) | (((cpu.e&0xF)==0xF)<<5);//halfcarry
			cpu.t = 4;
			break;
		}
		case 0x1E:	// LD E, n
		{
			cpu.e = cpu.mem.arr[cpu.pc++];
			cpu.t = 8;
			break;
		}
		case 0x1F:	// RR A
		{
			unsigned int carry = cpu.f & 0x10;
			cpu.f = (cpu.a & 0x01) << 4;
			cpu.a = (cpu.a >> 1) | (carry << 3);
			cpu.t = 4;
			break;
		}
		case 0x20:	// JR NZ, n
		{
			cpu.t = 8;
			if ((cpu.f & 0x80) == 0)
			{
				cpu.pc += ((char)(cpu.mem.arr[cpu.pc]))+1;
				cpu.t += 4;
			}
			else
				cpu.pc++;
			break;
		}
		case 0x21:	// LD HL, nn
		{
			cpu.l = cpu.mem.arr[cpu.pc++];
			cpu.h = cpu.mem.arr[cpu.pc++];
			cpu.t = 12;
			break;
		}
		case 0x22:	// LDI (HL), A
		{
			writeMemory(cpu.hl++, cpu.a);
			cpu.t = 8;
			break;
		}
		case 0x23:	// INC HL
		{
			cpu.hl++;
			cpu.t = 8;
			break;
		}
		case 0x24:	// INC H
		{
			cpu.h++;
			cpu.f = (cpu.f & 0x3F) | zeroTable[cpu.h];	// zero and sub flag
			cpu.f = (cpu.f & 0xDF) | ((!(cpu.h & 0xF)) << 5); //halfcarry
			cpu.t = 4;
			break;
		}
		case 0x25:	// DEC H
		{
			cpu.h--;
			cpu.f = (cpu.f & 0x7F) | zeroTable[cpu.h];	// zero flag
			cpu.f |= 0x40;	// subtract flag
			cpu.f = (cpu.f & 0xDF) | (((cpu.h&0xF)==0xF)<<5);	// halfcarry
			cpu.t = 4;
			break;
		}
		case 0x26:	// LD H,n
		{
			cpu.h = cpu.mem.arr[cpu.pc++];
			cpu.t = 8;
			break;
		}
		case 0x27:	// DAA
		{
			int a = cpu.a;
			// from GearBoy source
			if (!(cpu.f & 0x40))
			{
				if ((cpu.f & 0x20) || ((a & 0xF) > 9))
					a += 0x06;
				if ((cpu.f & 0x10) || (a > 0x9F))
					a += 0x60;
			}
			else
			{
				if (cpu.f & 0x20)
					a = (a-6) & 0xFF;
				if (cpu.f & 0x10)
					a -= 0x60;
			}

			cpu.f &= 0x50;
			if ((a & 0x100) == 0x100)
				cpu.f |= 0x10;
			a &= 0xFF;
			cpu.f |= zeroTable[a];
			cpu.a = a;
			cpu.t = 4;
			break;
		}
		case 0x28:	// JR Z, n
		{
			cpu.t = 8;
			if (cpu.f & 0x80)
			{
				cpu.pc += ((char)(cpu.mem.arr[cpu.pc]))+1;
				cpu.t += 4;
			}
			else
				cpu.pc++;
			break;
		}
		case 0x29:	// ADD HL, HL
		{
			unsigned int sum = cpu.hl * 2;
			cpu.f &= 0xBF;
			cpu.f = (cpu.f & 0xDF) |
						(((cpu.hl & 0xFFF) > (sum & 0xFFF)) << 5);
			cpu.f = (cpu.f & 0xEF) | ((sum > 0xFFFF) << 4);
			cpu.hl = sum;
			cpu.t = 8;
			break;
		}
		case 0x2A:	// LDI A, (HL)
		{
			cpu.a = cpu.mem.arr[cpu.hl];
			cpu.hl++;
			cpu.t = 8;
			break;
		}
		case 0x2B:	// DEC HL
		{
			cpu.hl--;
			cpu.t = 8;
			break;
		}
		case 0x2C:	// INC L
		{
			cpu.l++;
			cpu.f = (cpu.f & 0x3F) | zeroTable[cpu.l];	// zero and sub flag
			cpu.f = (cpu.f & 0xDF) | ((!(cpu.l & 0xF)) << 5); //halfcarry
			cpu.t = 4;
			break;
		}
		case 0x2D:	// DEC L
		{
			cpu.l--;
			cpu.f = (cpu.f & 0x7F) | zeroTable[cpu.l];	// zero flag
			cpu.f |= 0x40;	// subtract flag
			cpu.f = (cpu.f & 0xDF) | (((cpu.l&0xF)==0xF)<<5);//halfcarry
			cpu.t = 4;
			break;
		}
		case 0x2E:	// LD L,n
		{
			cpu.l = cpu.mem.arr[cpu.pc++];
			cpu.t = 8;
			break;
		}
		case 0x2F:	// CPL
		{
			cpu.a = ~(cpu.a);
			cpu.f |= 0x60;
			cpu.t = 4;
			break;
		}
		case 0x30:	// JR NC, n
		{
			cpu.t = 8;
			if ((cpu.f & 0x10) == 0)
			{
				cpu.pc += ((char)(cpu.mem.arr[cpu.pc]))+1;
				cpu.t += 4;
			}
			else
				cpu.pc++;
			break;
		}
		case 0x31:	// LD SP, nn
		{
			cpu.sp = cpu.mem.arr[cpu.pc+1];
			cpu.sp <<= 8;
			cpu.sp |= cpu.mem.arr[cpu.pc];
			cpu.pc += 2;
			cpu.t = 12;
			break;
		}
		case 0x32:	// LDD (HL), A
		{
			writeMemory(cpu.hl--, cpu.a);
			cpu.t = 8;
			break;
		}
		case 0x33:	// INC SP
		{
			cpu.sp++;
			cpu.t = 8;
			break;
		}
		case 0x34:	// INC (HL)
		{
			writeMemory(cpu.hl, cpu.mem.arr[cpu.hl]+1);
			unsigned int val = cpu.mem.arr[cpu.hl];
			cpu.f = (cpu.f & 0x3F) | zeroTable[val];	// zero and sub flag
			cpu.f = (cpu.f & 0xDF) | ((!(val & 0xF)) << 5); // halfcarry
			cpu.t = 12;
			break;
		}
		case 0x35:	// DEC (HL)
		{
			writeMemory(cpu.hl, cpu.mem.arr[cpu.hl]-1);
			unsigned int val = cpu.mem.arr[cpu.hl];
			cpu.f = (cpu.f & 0x7F) | zeroTable[val];	// zero flag
			cpu.f |= 0x40;	// subtract flag
			cpu.f = (cpu.f & 0xDF) | (((val&0xF)==0xF)<<5);//halfcarry
			cpu.t = 12;
			break;
		}
		case 0x36:	// LD (HL), n
		{
			writeMemory(cpu.hl, cpu.mem.arr[cpu.pc++]);
			cpu.t = 12;
			break;
		}
		case 0x37:	// SCF
		{
			cpu.f |= 0x10;
			cpu.f &= 0x9F;
			cpu.t = 4;
			break;
		}
		case 0x38:	// JR C, n
		{
			cpu.t = 8;
			if (cpu.f & 0x10)
			{
				cpu.pc += ((char)(cpu.mem.arr[cpu.pc]))+1;
				cpu.t += 4;
			}
			else
				cpu.pc++;
			break;
		}
		case 0x39:	// ADD HL, SP
		{
			unsigned int sum = cpu.hl + cpu.sp;
			cpu.f &= 0xBF;
			cpu.f = (cpu.f & 0xDF) |
						(((cpu.hl & 0xFFF) > (sum & 0xFFF)) << 5);
			cpu.f = (cpu.f & 0xEF) | ((sum > 0xFFFF) << 4);
			cpu.hl = sum;
			cpu.t = 8;
			break;
		}
		case 0x3A:	// LDD A, (HL)
		{
			cpu.a = cpu.mem.arr[cpu.hl--];
			cpu.t = 8;
			break;
		}
		case 0x3B:	// DEC SP
		{
			cpu.sp--;
			cpu.t = 8;
			break;
		}
		case 0x3C:	// INC A
		{
			cpu.a++;
			cpu.f = (cpu.f & 0x3F) | zeroTable[cpu.a];	// zero and sub flag
			cpu.f = (cpu.f & 0xDF) | ((!(cpu.a & 0xF)) << 5); //halfcarry
			cpu.t = 4;
			break;
		}
		case 0x3D:	// DEC A
		{
			cpu.a--;
			cpu.f = (cpu.f & 0x7F) | zeroTable[cpu.a];	// zero flag
			cpu.f |= 0x40;	// subtract flag
			cpu.f = (cpu.f & 0xDF) | (((cpu.a&0xF)==0xF)<<5);//halfcarry
			cpu.t = 4;
			break;
		}
		case 0x3E:	// LD A, n
		{
			cpu.a = cpu.mem.arr[cpu.pc++];
			cpu.t = 8;
			break;
		}
		case 0x3F:	// CCF
		{
			cpu.f ^= 0x10;	// invert carry flag
			cpu.f &= 0x9F;
			cpu.t = 4;
			break;
		}
		case 0x40:	// LD B, B
		{
			cpu.t = 4;
			break;
		}
		case 0x41:	// LD B, C
		{
			cpu.b = cpu.c;
			cpu.t = 4;
			break;
		}
		case 0x42:	// LD B, D
		{
			cpu.b = cpu.d;
			cpu.t = 4;
			break;
		}
		case 0x43:	// LD B, E
		{
			cpu.b = cpu.e;
			cpu.t = 4;
			break;
		}
		case 0x44:	// LD B, H
		{
			cpu.b = cpu.h;
			cpu.t = 4;
			break;
		}
		case 0x45:	// LD B, L
		{
			cpu.b = cpu.l;
			cpu.t = 4;
			break;
		}
		case 0x46:	// LD B, (HL)
		{
			cpu.b = cpu.mem.arr[cpu.hl];
			cpu.t = 8;
			break;
		}
		case 0x47:	// LD B, A
		{
			cpu.b = cpu.a;
			cpu.t = 4;
			break;
		}
		case 0x48:	// LD C, B
		{
			cpu.c = cpu.b;
			cpu.t = 4;
			break;
		}
		case 0x49:	// LD C, C
		{
			cpu.t = 4;
			break;
		}
		case 0x4A:	// LD C, D
		{
			cpu.c = cpu.d;
			cpu.t = 4;
			break;
		}
		case 0x4B:	// LD C, E
		{
			cpu.c = cpu.e;
			cpu.t = 4;
			break;
		}
		case 0x4C:	// LD C, H
		{
			cpu.c = cpu.h;
			cpu.t = 4;
			break;
		}
		case 0x4D:	// LD C, L
		{
			cpu.c = cpu.l;
			cpu.t = 4;
			break;
		}
		case 0x4E:	// LD C, (HL)
		{
			cpu.c = cpu.mem.arr[cpu.hl];
			cpu.t = 8;
			break;
		}
		case 0x4F:	// LD C, A
		{
			cpu.c = cpu.a;
			cpu.t = 4;
			break;
		}
		case 0x50:	// LD D, B
		{
			cpu.d = cpu.b;
			cpu.t = 4;
			break;
		}
		case 0x51:	// LD D, C
		{
			cpu.d = cpu.c;
			cpu.t = 4;
			break;
		}
		case 0x52:	// LD D, D
		{
			cpu.t = 4;
			break;
		}
		case 0x53:	// LD D, E
		{
			cpu.d = cpu.e;
			cpu.t = 4;
			break;
		}
		case 0x54:	// LD D, H
		{
			cpu.d = cpu.h;
			cpu.t = 4;
			break;
		}
		case 0x55:	// LD D, L
		{
			cpu.d = cpu.l;
			cpu.t = 4;
			break;
		}
		case 0x56:	// LD D, (HL)
		{
			cpu.d = cpu.mem.arr[cpu.hl];
			cpu.t = 8;
			break;
		}
		case 0x57:	// LD D, A
		{
			cpu.d = cpu.a;
			cpu.t = 4;
			break;
		}
		case 0x58:	// LD E, B
		{
			cpu.e = cpu.b;
			cpu.t = 4;
			break;
		}
		case 0x59:	// LD E, C
		{
			cpu.e = cpu.c;
			cpu.t = 4;
			break;
		}
		case 0x5A:	// LD E, D
		{
			cpu.e = cpu.d;
			cpu.t = 4;
			break;
		}
		case 0x5B:	// LD E, E
		{
			cpu.t = 4;
			break;
		}
		case 0x5C:	// LD E, H
		{
			cpu.e = cpu.h;
			cpu.t = 4;
			break;
		}
		case 0x5D:	// LD E, L
		{
			cpu.e = cpu.l;
			cpu.t = 4;
			break;
		}
		case 0x5E:	// LD E, (HL)
		{
			cpu.e = cpu.mem.arr[cpu.hl];
			cpu.t = 8;
			break;
		}
		case 0x5F:	// LD E, A
		{
			cpu.e = cpu.a;
			cpu.t = 4;
			break;
		}
		case 0x60:	// LD H, B
		{
			cpu.h = cpu.b;
			cpu.t = 4;
			break;
		}
		case 0x61:	// LD H, C
		{
			cpu.h = cpu.c;
			cpu.t = 4;
			break;
		}
		case 0x62:	// LD H, D
		{
			cpu.h = cpu.d;
			cpu.t = 4;
			break;
		}
		case 0x63:	// LD H, E
		{
			cpu.h = cpu.e;
			cpu.t = 4;
			break;
		}
		case 0x64:	// LD H, H
		{
			cpu.t = 4;
			break;
		}
		case 0x65:	// LD H, L
		{
			cpu.h = cpu.l;
			cpu.t = 4;
			break;
		}
		case 0x66:	// LD H, (HL)
		{
			cpu.h = cpu.mem.arr[cpu.hl];
			cpu.t = 8;
			break;
		}
		case 0x67:	// LD H, A
		{
			cpu.h = cpu.a;
			cpu.t = 4;
			break;
		}
		case 0x68:	// LD L, B
		{
			cpu.l = cpu.b;
			cpu.t = 4;
			break;
		}
		case 0x69:	// LD L, C
		{
			cpu.l = cpu.c;
			cpu.t = 4;
			break;
		}
		case 0x6A:	// LD L, D
		{
			cpu.l = cpu.d;
			cpu.t = 4;
			break;
		}
		case 0x6B:	// LD L, E
		{
			cpu.l = cpu.e;
			cpu.t = 4;
			break;
		}
		case 0x6C:	// LD L, H
		{
			cpu.l = cpu.h;
			cpu.t = 4;
			break;
		}
		case 0x6D:	// LD L, L
		{
			cpu.t = 4;
			break;
		}
		case 0x6E:	// LD L, (HL)
		{
			cpu.l = cpu.mem.arr[cpu.hl];
			cpu.t = 8;
			break;
		}
		case 0x6F:	// LD L, A
		{
			cpu.l = cpu.a;
			cpu.t = 4;
			break;
		}
		case 0x70:	// LD (HL), B
		{
			writeMemory(cpu.hl, cpu.b);
			cpu.t = 8;
			break;
		}
		case 0x71:	// LD (HL), C
		{
			writeMemory(cpu.hl, cpu.c);
			cpu.t = 8;
			break;
		}
		case 0x72:	// LD (HL), D
		{
			writeMemory(cpu.hl, cpu.d);
			cpu.t = 8;
			break;
		}
		case 0x73:	// LD (HL), E
		{
			writeMemory(cpu.hl, cpu.e);
			cpu.t = 8;
			break;
		}
		case 0x74:	// LD (HL), H
		{
			writeMemory(cpu.hl, cpu.h);
			cpu.t = 8;
			break;
		}
		case 0x75:	// LD (HL), L
		{
			writeMemory(cpu.hl, cpu.l);
			cpu.t = 8;
			break;
		}
		case 0x76:	// HALT
		{
			// strategy according to gbman v1.0
			cpu.halted = true;

			if (!cpu.masterInterrupt)
			{
				cpu.haltBug = true;
			}

			/*if (cpu.masterInterrupt)
				cpu.halted = true;
			else
				cpu.haltBug = true;

			if (cpu.IMEOnReq)
			{
				cpu.masterInterrupt = true;
				cpu.IMEOnReq = false;
			}
			else if (cpu.IMEOffReq)	// check if this is needed
			{
				cpu.masterInterrupt = false;
				cpu.IMEOffReq = false;

				if (cpu.halted)		// trying this out
				{
					cpu.halted = false;
					cpu.haltBug = true;
				}
			}*/
			cpu.t = 4;
			break;
		}
		case 0x77:	// LD (HL), A
		{
			writeMemory(cpu.hl, cpu.a);
			cpu.t = 8;
			break;
		}
		case 0x78:	// LD A, B
		{
			cpu.a = cpu.b;
			cpu.t = 4;
			break;
		}
		case 0x79:	// LD A, C
		{
			cpu.a = cpu.c;
			cpu.t = 4;
			break;
		}
		case 0x7A:	// LD A, D
		{
			cpu.a = cpu.d;
			cpu.t = 4;
			break;
		}
		case 0x7B:	// LD A, E
		{
			cpu.a = cpu.e;
			cpu.t = 4;
			break;
		}
		case 0x7C:	// LD A, H
		{
			cpu.a = cpu.h;
			cpu.t = 4;
			break;
		}
		case 0x7D:	// LD A, L
		{
			cpu.a = cpu.l;
			cpu.t = 4;
			break;
		}
		case 0x7E:	// LD A, (HL)
		{
			cpu.a = cpu.mem.arr[cpu.hl];
			cpu.t = 8;
			break;
		}
		case 0x7F:	// LD A, A
		{
			cpu.t = 4;
			break;
		}
		case 0x80:	// ADD A, B
		{
			unsigned int sum = cpu.a + cpu.b;
			cpu.f = zeroTable[sum & 0xFF] |
				(((cpu.a & 0xF) > (sum & 0xF)) << 5) |
				((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 4;
			break;
		}
		case 0x81:	// ADD A, C
		{
			unsigned int sum = cpu.a + cpu.c;
			cpu.f = zeroTable[sum & 0xFF] |
				(((cpu.a & 0xF) > (sum & 0xF)) << 5) |
				((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 4;
			break;
		}
		case 0x82:	// ADD A, D
		{
			unsigned int sum = cpu.a + cpu.d;
			cpu.f = zeroTable[sum & 0xFF] |
				(((cpu.a & 0xF) > (sum & 0xF)) << 5) |
				((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 4;
			break;
		}
		case 0x83:	// ADD A, E
		{
			unsigned int sum = cpu.a + cpu.e;
			cpu.f = zeroTable[sum & 0xFF] |
				(((cpu.a & 0xF) > (sum & 0xF)) << 5) |
				((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 4;
			break;
		}
		case 0x84:	// ADD A, H
		{
			unsigned int sum = cpu.a + cpu.h;
			cpu.f = zeroTable[sum & 0xFF] |
				(((cpu.a & 0xF) > (sum & 0xF)) << 5) |
				((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 4;
			break;
		}
		case 0x85:	// ADD A, L
		{
			unsigned int sum = cpu.a + cpu.l;
			cpu.f = zeroTable[sum & 0xFF] |
				(((cpu.a & 0xF) > (sum & 0xF)) << 5) |
				((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 4;
			break;
		}
		case 0x86:	// ADD A, (HL)
		{
			unsigned int sum = cpu.a + cpu.mem.arr[cpu.hl];
			cpu.f = zeroTable[sum & 0xFF] |
				(((cpu.a & 0xF) > (sum & 0xF)) << 5) |
				((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 8;
			break;
		}
		case 0x87:	// ADD A, A
		{
			unsigned int sum = cpu.a + cpu.a;
			cpu.f = zeroTable[sum & 0xFF] |
				(((cpu.a & 0xF) > (sum & 0xF)) << 5) |
				((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 4;
			break;
		}
		case 0x88:	// ADC A, B
		{
			int carry = ((cpu.f & 0x10)?1:0);
			unsigned int sum = cpu.a + cpu.b + carry;
			cpu.f = zeroTable[sum & 0xFF] |
				(((cpu.a & 0xF) + (cpu.b & 0xF) + carry > 0xF) << 5) |
				((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 4;
			break;
		}
		case 0x89:	// ADC A, C
		{
			int carry = ((cpu.f & 0x10)?1:0);
			unsigned int sum = cpu.a + cpu.c + carry;
			cpu.f = zeroTable[sum & 0xFF] |
				(((cpu.a & 0xF) + (cpu.c & 0xF) + carry > 0xF) << 5) |
				((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 4;
			break;
		}
		case 0x8A:	// ADC A, D
		{
			int carry = ((cpu.f & 0x10)?1:0);
			unsigned int sum = cpu.a + cpu.d + carry;
			cpu.f = zeroTable[sum & 0xFF] |
				(((cpu.a & 0xF) + (cpu.d & 0xF) + carry > 0xF) << 5) |
				((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 4;
			break;
		}
		case 0x8B:	// ADC A, E
		{
			int carry = ((cpu.f & 0x10)?1:0);
			unsigned int sum = cpu.a + cpu.e + carry;
			cpu.f = zeroTable[sum & 0xFF] |
				(((cpu.a & 0xF) + (cpu.e & 0xF) + carry > 0xF) << 5) |
				((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 4;
			break;
		}
		case 0x8C:	// ADC A, H
		{
			int carry = ((cpu.f & 0x10)?1:0);
			unsigned int sum = cpu.a + cpu.h + carry;
			cpu.f = zeroTable[sum & 0xFF] |
				(((cpu.a & 0xF) + (cpu.h & 0xF) + carry > 0xF) << 5) |
				((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 4;
			break;
		}
		case 0x8D:	// ADC A, L
		{
			int carry = ((cpu.f & 0x10)?1:0);
			unsigned int sum = cpu.a + cpu.l + carry;
			cpu.f = zeroTable[sum & 0xFF] |
				(((cpu.a & 0xF) + (cpu.l & 0xF) + carry > 0xF) << 5) |
				((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 4;
			break;
		}
		case 0x8E:	// ADC A, (HL)
		{
			int carry = ((cpu.f & 0x10) ? 1 : 0);
			unsigned int sum = cpu.a + cpu.mem.arr[cpu.hl] + carry;
			cpu.f = zeroTable[sum & 0xFF] |
				(((cpu.a & 0xF) + (cpu.mem.arr[cpu.hl] & 0xF)+carry > 0xF) << 5) |
				((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 8;
			break;
		}
		case 0x8F:	// ADC A, A
		{
			int carry = ((cpu.f & 0x10)?1:0);
			unsigned int sum = cpu.a + cpu.a + carry;
			cpu.f = zeroTable[sum & 0xFF] |
				(((cpu.a & 0xF) + (cpu.a & 0xF) + carry > 0xF) << 5) |
				((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 4;
			break;
		}
		case 0x90:	// SUB A, B
		{
			int sub = cpu.a - cpu.b;
			cpu.f = zeroTable[sub & 0xFF] |
				(((cpu.a & 0xF) < (sub & 0xF)) << 5) |
				((sub < 0) << 4) | 0x40;
			cpu.a = sub;
			cpu.t = 4;
			break;
		}
		case 0x91:	// SUB A, C
		{
			int sub = cpu.a - cpu.c;
			cpu.f = zeroTable[sub & 0xFF] |
				(((cpu.a & 0xF) < (sub & 0xF)) << 5) |
				((sub < 0) << 4) | 0x40;
			cpu.a = sub;
			cpu.t = 4;
			break;
		}
		case 0x92:	// SUB A, D
		{
			int sub = cpu.a - cpu.d;
			cpu.f = zeroTable[sub & 0xFF] |
				(((cpu.a & 0xF) < (sub & 0xF)) << 5) |
				((sub < 0) << 4) | 0x40;
			cpu.a = sub;
			cpu.t = 4;
			break;
		}
		case 0x93:	// SUB A, E
		{
			int sub = cpu.a - cpu.e;
			cpu.f = zeroTable[sub & 0xFF] |
				(((cpu.a & 0xF) < (sub & 0xF)) << 5) |
				((sub < 0) << 4) | 0x40;
			cpu.a = sub;
			cpu.t = 4;
			break;
		}
		case 0x94:	// SUB A, H
		{
			int sub = cpu.a - cpu.h;
			cpu.f = zeroTable[sub & 0xFF] |
				(((cpu.a & 0xF) < (sub & 0xF)) << 5) |
				((sub < 0) << 4) | 0x40;
			cpu.a = sub;
			cpu.t = 4;
			break;
		}
		case 0x95:	// SUB A, L
		{
			int sub = cpu.a - cpu.l;
			cpu.f = zeroTable[sub & 0xFF] |
				(((cpu.a & 0xF) < (sub & 0xF)) << 5) |
				((sub < 0) << 4) | 0x40;
			cpu.a = sub;
			cpu.t = 4;
			break;
		}
		case 0x96:	// SUB A, (HL)
		{
			int sub = cpu.a - cpu.mem.arr[cpu.hl];
			cpu.f = zeroTable[sub & 0xFF] |
				(((cpu.a & 0xF) < (sub & 0xF)) << 5) |
				((sub < 0) << 4) | 0x40;
			cpu.a = sub;
			cpu.t = 8;
			break;
		}
		case 0x97:	// SUB A, A
		{
			int sub = 0;
			cpu.f = zeroTable[sub & 0xFF] |
				(((cpu.a & 0xF) < (sub & 0xF)) << 5) |
				((sub < 0) << 4) | 0x40;
			cpu.a = sub;
			cpu.t = 4;
			break;
		}
		case 0x98:	// SBC A, B
		{
			int carry = (cpu.f & 0x10)?1:0;
			int sub = cpu.a - cpu.b - carry;
			cpu.f = zeroTable[(reg8)sub & 0xFF] | 
				((sub < 0) << 4) | 0x40;
			if ((cpu.a & 0xF) - (cpu.b & 0xF) - carry < 0)
				cpu.f |= 0x20;
			cpu.a = (reg8)sub;
			cpu.t = 4;
			break;
		}
		case 0x99:	// SBC A, C
		{
			int carry = ((cpu.f & 0x10)?1:0);
			int sub = cpu.a - cpu.c - carry;
			cpu.f = zeroTable[sub & 0xFF] | 
				((sub < 0) << 4) | 0x40;
			if ((cpu.a & 0xF) - (cpu.c & 0xF) - carry < 0)
				cpu.f |= 0x20;
			cpu.a = sub;
			cpu.t = 4;
			break;
		}
		case 0x9A:	// SBC A, D
		{
			int carry = ((cpu.f & 0x10)?1:0);
			int sub = cpu.a - cpu.d - carry;
			cpu.f = zeroTable[sub & 0xFF] | 
				((sub < 0) << 4) | 0x40;
			if ((cpu.a & 0xF) - (cpu.d & 0xF) - carry < 0)
				cpu.f |= 0x20;
			cpu.a = sub;
			cpu.t = 4;
			break;
		}
		case 0x9B:	// SBC A, E
		{
			int carry = ((cpu.f & 0x10)?1:0);
			int sub = cpu.a - cpu.e - carry;
			cpu.f = zeroTable[sub & 0xFF] | 
				((sub < 0) << 4) | 0x40;
			if ((cpu.a & 0xF) - (cpu.e & 0xF) - carry < 0)
				cpu.f |= 0x20;
			cpu.a = sub;
			cpu.t = 4;
			break;
		}
		case 0x9C:	// SBC A, H
		{
			int carry = ((cpu.f & 0x10)?1:0);
			int sub = cpu.a - cpu.h - carry;
			cpu.f = zeroTable[sub & 0xFF] | 
				((sub < 0) << 4) | 0x40;
			if ((cpu.a & 0xF) - (cpu.h & 0xF) - carry < 0)
				cpu.f |= 0x20;
			cpu.a = sub;
			cpu.t = 4;
			break;
		}
		case 0x9D:	// SBC A, L
		{
			int carry = ((cpu.f & 0x10)?1:0);
			int sub = cpu.a - cpu.l - carry;
			cpu.f = zeroTable[sub & 0xFF] | 
				((sub < 0) << 4) | 0x40;
			if ((cpu.a & 0xF) - (cpu.l & 0xF) - carry < 0)
				cpu.f |= 0x20;
			cpu.a = sub;
			cpu.t = 4;
			break;
		}
		case 0x9E:	// SBC A, (HL)
		{
			int carry = ((cpu.f & 0x10)?1:0);
			int sub = cpu.a - cpu.mem.arr[cpu.hl] - carry;
			cpu.f = zeroTable[sub & 0xFF] | 
				((sub < 0) << 4) | 0x40;
			if ((cpu.a & 0xF) - (cpu.mem.arr[cpu.hl] & 0xF) -
					carry < 0)
				cpu.f |= 0x20;
			cpu.a = sub;
			cpu.t = 8;
			break;
		}
		case 0x9F:	// SBC A, A
		{
			int carry = ((cpu.f & 0x10)?1:0);
			int sub = 0 - carry;
			cpu.f = zeroTable[sub & 0xFF] | 
				((sub < 0) << 4) | 0x40;
			if (0 - carry < 0)
				cpu.f |= 0x20;
			cpu.a = sub;
			cpu.t = 4;
			break;
		}
		case 0xA0:	// AND B
		{
			cpu.a &= cpu.b;
			cpu.f = zeroTable[cpu.a] | 0x20;
			cpu.t = 4;
			break;
		}
		case 0xA1:	// AND C
		{
			cpu.a &= cpu.c;
			cpu.f = zeroTable[cpu.a] | 0x20;
			cpu.t = 4;
			break;
		}
		case 0xA2:	// AND D
		{
			cpu.a &= cpu.d;
			cpu.f = zeroTable[cpu.a] | 0x20;
			cpu.t = 4;
			break;
		}
		case 0xA3:	// AND E
		{
			cpu.a &= cpu.e;
			cpu.f = zeroTable[cpu.a] | 0x20;
			cpu.t = 4;
			break;
		}
		case 0xA4:	// AND H
		{
			cpu.a &= cpu.h;
			cpu.f = zeroTable[cpu.a] | 0x20;
			cpu.t = 4;
			break;
		}
		case 0xA5:	// AND L
		{
			cpu.a &= cpu.l;
			cpu.f = zeroTable[cpu.a] | 0x20;
			cpu.t = 4;
			break;
		}
		case 0xA6:	// AND (HL)
		{
			cpu.a &= cpu.mem.arr[cpu.hl];
			cpu.f = zeroTable[cpu.a] | 0x20;
			cpu.t = 8;
			break;
		}
		case 0xA7:	// AND A
		{
			cpu.f = zeroTable[cpu.a] | 0x20;
			cpu.t = 4;
			break;
		}
		case 0xA8:	// XOR B
		{
			cpu.a ^= cpu.b;
			cpu.f = zeroTable[cpu.a];
			cpu.t = 4;
			break;
		}
		case 0xA9:	// XOR C
		{
			cpu.a ^= cpu.c;
			cpu.f = zeroTable[cpu.a];
			cpu.t = 4;
			break;
		}
		case 0xAA:	// XOR D
		{
			cpu.a ^= cpu.d;
			cpu.f = zeroTable[cpu.a];
			cpu.t = 4;
			break;
		}
		case 0xAB:	// XOR E
		{
			cpu.a ^= cpu.e;
			cpu.f = zeroTable[cpu.a];
			cpu.t = 4;
			break;
		}
		case 0xAC:	// XOR H
		{
			cpu.a ^= cpu.h;
			cpu.f = zeroTable[cpu.a];
			cpu.t = 4;
			break;
		}
		case 0xAD:	// XOR L
		{
			cpu.a ^= cpu.l;
			cpu.f = zeroTable[cpu.a];
			cpu.t = 4;
			break;
		}
		case 0xAE:	// XOR (HL)
		{
			cpu.a ^= cpu.mem.arr[cpu.hl];
			cpu.f = zeroTable[cpu.a];
			cpu.t = 8;
			break;
		}
		case 0xAF:	// XOR A
		{
			cpu.a = 0;
			cpu.f = 0x80;
			cpu.t = 4;
			break;
		}
		case 0xB0:	// OR B
		{
			cpu.a |= cpu.b;
			cpu.f = zeroTable[cpu.a];
			cpu.t = 4;
			break;
		}
		case 0xB1:	// OR C
		{
			cpu.a |= cpu.c;
			cpu.f = zeroTable[cpu.a];
			cpu.t = 4;
			break;
		}
		case 0xB2:	// OR D
		{
			cpu.a |= cpu.d;
			cpu.f = zeroTable[cpu.a];
			cpu.t = 4;
			break;
		}
		case 0xB3:	// OR E
		{
			cpu.a |= cpu.e;
			cpu.f = zeroTable[cpu.a];
			cpu.t = 4;
			break;
		}
		case 0xB4:	// OR H
		{
			cpu.a |= cpu.h;
			cpu.f = zeroTable[cpu.a];
			cpu.t = 4;
			break;
		}
		case 0xB5:	// OR L
		{
			cpu.a |= cpu.l;
			cpu.f = zeroTable[cpu.a];
			cpu.t = 4;
			break;
		}
		case 0xB6:	// OR (HL)
		{
			cpu.a |= cpu.mem.arr[cpu.hl];
			cpu.f = zeroTable[cpu.a];
			cpu.t = 8;
			break;
		}
		case 0xB7:	// OR A
		{
			cpu.f = zeroTable[cpu.a];
			cpu.t = 4;
			break;
		}
		case 0xB8:	// CP B
		{
			int sub = cpu.a - cpu.b;
			cpu.f = zeroTable[sub&0xFF] | 0x40 |
						(((sub & 0xF) > (cpu.a & 0xF))<<5) |
						((sub < 0) << 4);
			cpu.t = 4;
			break;
		}
		case 0xB9:	// CP C
		{
			int sub = cpu.a - cpu.c;
			cpu.f = zeroTable[sub&0xFF] | 0x40 |
						(((sub & 0xF) > (cpu.a & 0xF))<<5) |
						((sub < 0) << 4);
			cpu.t = 4;
			break;
		}
		case 0xBA:	// CP D
		{
			int sub = cpu.a - cpu.d;
			cpu.f = zeroTable[sub&0xFF] | 0x40 |
						(((sub & 0xF) > (cpu.a & 0xF))<<5) |
						((sub < 0) << 4);
			cpu.t = 4;
			break;
		}
		case 0xBB:	// CP E
		{
			int sub = cpu.a - cpu.e;
			cpu.f = zeroTable[sub&0xFF] | 0x40 |
						(((sub & 0xF) > (cpu.a & 0xF))<<5) |
						((sub < 0) << 4);
			cpu.t = 4;
			break;
		}
		case 0xBC:	// CP H
		{
			int sub = cpu.a - cpu.h;
			cpu.f = zeroTable[sub&0xFF] | 0x40 |
						(((sub & 0xF) > (cpu.a & 0xF))<<5) |
						((sub < 0) << 4);
			cpu.t = 4;
			break;
		}
		case 0xBD:	// CP L
		{
			int sub = cpu.a - cpu.l;
			cpu.f = zeroTable[sub&0xFF] | 0x40 |
						(((sub & 0xF) > (cpu.a & 0xF))<<5) |
						((sub < 0) << 4);
			cpu.t = 4;
			break;
		}
		case 0xBE:	// CP (HL)
		{
			int sub = cpu.a - cpu.mem.arr[cpu.hl];
			cpu.f = zeroTable[sub&0xFF] | 0x40 |
						(((sub & 0xF) > (cpu.a & 0xF))<<5) |
						((sub < 0) << 4);
			cpu.t = 8;
			break;
		}
		case 0xBF:	// CP A
		{
			cpu.f = 0x80 | 0x40;
			cpu.t = 4;
			break;
		}
		case 0xC0:	// RET NZ
		{
			cpu.t = 8;
			if (!(cpu.f & 0x80))
			{
				cpu.pc = cpu.mem.arr[cpu.sp+1];
				cpu.pc = cpu.pc << 8;
				cpu.pc |= cpu.mem.arr[cpu.sp];
				cpu.sp += 2;
				cpu.t += 12;
			}
			break;
		}
		case 0xC1:	// POP BC
		{
			cpu.c = cpu.mem.arr[cpu.sp++];
			cpu.b = cpu.mem.arr[cpu.sp++];
			cpu.t = 12;
			break;
		}
		case 0xC2:	// JP NZ, nn
		{
			cpu.t = 12;
			if (!(cpu.f & 0x80))
			{
				unsigned int addr = cpu.mem.arr[cpu.pc+1];
				addr = addr << 8;
				addr |= cpu.mem.arr[cpu.pc];
				cpu.pc = addr;
				cpu.t += 4;
			}
			else
				cpu.pc += 2;
			break;
		}
		case 0xC3:	// JP nn
		{
			unsigned int addr = cpu.mem.arr[cpu.pc+1];
			addr = addr << 8;
			addr |= cpu.mem.arr[cpu.pc];
			cpu.pc = addr;
			cpu.t = 16;
			break;
		}
		case 0xC4:	// CALL NZ, nn
		{
			cpu.t = 12;
			if (!(cpu.f & 0x80))
			{
				unsigned int tempPc = cpu.mem.arr[cpu.pc+1];
				tempPc = tempPc << 8;
				tempPc |= cpu.mem.arr[cpu.pc];
				cpu.pc += 2;
				cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc >> 8);
				cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc);
				cpu.pc = tempPc;
				cpu.t += 12;
			}
			else
				cpu.pc += 2;
			break;
		}
		case 0xC5:	// PUSH BC
		{
			cpu.mem.arr[--(cpu.sp)] = cpu.b;
			cpu.mem.arr[--(cpu.sp)] = cpu.c;
			cpu.t = 16;
			break;
		}
		case 0xC6:	// ADD A, n
		{
			unsigned int sum = cpu.a + cpu.mem.arr[cpu.pc++];
			cpu.f = zeroTable[sum & 0xFF] | (((cpu.a & 0xF) > (sum&0xF))<<5)
					| ((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 8;
			break;
		}
		case 0xC7:	// RST 0
		{
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc >> 8);
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc);
			cpu.pc = 0;
			cpu.t = 16;
			break;
		}
		case 0xC8:	// RET Z
		{
			cpu.t = 8;
			if (cpu.f & 0x80)
			{
				cpu.pc = cpu.mem.arr[cpu.sp+1];
				cpu.pc = cpu.pc << 8;
				cpu.pc |= cpu.mem.arr[cpu.sp];
				cpu.sp += 2;
				cpu.t += 12;
			}
			break;
		}
		case 0xC9:	// RET
		{
			cpu.t = 16;
			cpu.pc = cpu.mem.arr[cpu.sp+1];
			cpu.pc = cpu.pc << 8;
			cpu.pc |= cpu.mem.arr[cpu.sp];
			cpu.sp += 2;
			break;
		}
		case 0xCA:	// JP Z, nn
		{
			cpu.t = 12;
			if (cpu.f & 0x80)
			{
				unsigned int addr = cpu.mem.arr[cpu.pc+1];
				addr = addr << 8;
				addr |= cpu.mem.arr[cpu.pc];
				cpu.pc = addr;
				cpu.t += 4;
			}
			else
				cpu.pc += 2;
			break;
		}
		case 0xCB:	// Ext ops
		{
			ExecCB();
			break;
		}
		case 0xCC:	// CALL Z, nn
		{
			cpu.t = 12;
			if (cpu.f & 0x80)
			{
				unsigned int tempPc = cpu.mem.arr[cpu.pc+1];
				tempPc = tempPc << 8;
				tempPc |= cpu.mem.arr[cpu.pc];
				cpu.pc += 2;
				cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc >> 8);
				cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc);
				cpu.pc = tempPc;
				cpu.t += 12;
			}
			else
				cpu.pc += 2;
			break;
		}
		case 0xCD:	// CALL nn
		{
			cpu.t = 24;
			unsigned int tempPc = cpu.mem.arr[cpu.pc+1];
			tempPc = tempPc << 8;
			tempPc |= cpu.mem.arr[cpu.pc];
			cpu.pc += 2;
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc >> 8);
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc);
			cpu.pc = tempPc;
			break;
		}
		case 0xCE:	// ADC A, n
		{
			int carry = ((cpu.f & 0x10)?1:0);
			unsigned int n = cpu.mem.arr[cpu.pc++];
			unsigned int sum = cpu.a + n + carry;
			cpu.f = zeroTable[sum & 0xFF] |
				(((cpu.a & 0xF) + (n & 0xF) + 
					carry > 0x0F) << 5) |
				((sum > 0xFF) << 4);
			cpu.a = sum;
			cpu.t = 8;
			break;
		}
		case 0xCF:	// RST 8
		{
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc >> 8);
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc);
			cpu.pc = 8;
			cpu.t = 16;
			break;
		}
		case 0xD0:	// RET NC
		{
			cpu.t = 8;
			if (!(cpu.f & 0x10))
			{
				cpu.pc = cpu.mem.arr[cpu.sp+1];
				cpu.pc = cpu.pc << 8;
				cpu.pc |= cpu.mem.arr[cpu.sp];
				cpu.sp += 2;
				cpu.t += 12;
			}
			break;
		}
		case 0xD1:	// POP DE
		{
			cpu.e = cpu.mem.arr[cpu.sp++];
			cpu.d = cpu.mem.arr[cpu.sp++];
			cpu.t = 12;
			break;
		}
		case 0xD2:	// JP NC, nn
		{
			cpu.t = 12;
			if (!(cpu.f & 0x10))
			{
				unsigned int addr = cpu.mem.arr[cpu.pc+1];
				addr = addr << 8;
				addr |= cpu.mem.arr[cpu.pc];
				cpu.pc = addr;
				cpu.t += 4;
			}
			else
				cpu.pc += 2;
			break;
		}
		case 0xD3:	// Non existent op
		{
			break;
		}
		case 0xD4:	// CALL NC, nn
		{
			cpu.t = 12;
			if (!(cpu.f & 0x10))
			{
				unsigned int tempPc = cpu.mem.arr[cpu.pc+1];
				tempPc = tempPc << 8;
				tempPc |= cpu.mem.arr[cpu.pc];
				cpu.pc += 2;
				cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc >> 8);
				cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc);
				cpu.pc = tempPc;
				cpu.t += 12;
			}
			else
				cpu.pc += 2;
			break;
		}
		case 0xD5:	// PUSH DE
		{
			cpu.mem.arr[--(cpu.sp)] = cpu.d;
			cpu.mem.arr[--(cpu.sp)] = cpu.e;
			cpu.t = 16;
			break;
		}
		case 0xD6:	// SUB A, n
		{
			int sub = cpu.a - cpu.mem.arr[cpu.pc++];
			cpu.f = zeroTable[sub & 0xFF] | (((cpu.a & 0xF) < (sub&0xF))<<5)
					| ((sub < 0) << 4) | 0x40;
			cpu.a = sub;
			cpu.t = 8;
			break;
		}
		case 0xD7:	// RST 10
		{
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc >> 8);
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc);
			cpu.pc = 0x0010;
			cpu.t = 16;
			break;
		}
		case 0xD8:	// RET C
		{
			cpu.t = 8;
			if (cpu.f & 0x10)
			{
				cpu.pc = cpu.mem.arr[cpu.sp+1];
				cpu.pc = cpu.pc << 8;
				cpu.pc |= cpu.mem.arr[cpu.sp];
				cpu.sp += 2;
				cpu.t += 12;
			}
			break;
		}
		case 0xD9:	// RETI
		{
			cpu.t = 16;
			cpu.pc = cpu.mem.arr[cpu.sp+1];
			cpu.pc = cpu.pc << 8;
			cpu.pc |= cpu.mem.arr[cpu.sp];
			cpu.sp += 2;
			cpu.masterInterrupt = true;
			break;
		}
		case 0xDA:	// JP C, nn
		{
			cpu.t = 12;
			if (cpu.f & 0x10)
			{
				unsigned int addr = cpu.mem.arr[cpu.pc+1];
				addr = addr << 8;
				addr |= cpu.mem.arr[cpu.pc];
				cpu.pc = addr;
				cpu.t += 4;
			}
			else
				cpu.pc += 2;
			break;
		}
		case 0xDB:	// Non existent op
		{
			break;
		}
		case 0xDC:	// CALL C, nn
		{
			cpu.t = 12;
			if (cpu.f & 0x10)
			{
				unsigned int tempPc = cpu.mem.arr[cpu.pc+1];
				tempPc = tempPc << 8;
				tempPc |= cpu.mem.arr[cpu.pc];
				cpu.pc += 2;
				cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc >> 8);
				cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc);
				cpu.pc = tempPc;
				cpu.t += 12;
			}
			else
				cpu.pc += 2;
			break;
		}
		case 0xDD:	// Non existent op
		{
			break;
		}
		case 0xDE:	// SBC A, n
		{
			int carry = ((cpu.f & 0x10)?1:0);
			int n = cpu.mem.arr[cpu.pc++];
			int sub = cpu.a - n - carry;
			cpu.f = zeroTable[sub & 0xFF] | 
				((sub < 0) << 4) | 0x40;
			if ((cpu.a & 0xF) - (n & 0xF) - carry < 0)
				cpu.f |= 0x20;
			cpu.a = (unsigned char)sub;
			cpu.t = 8;
			break;
		}
		case 0xDF:	// RST 18
		{
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc >> 8);
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc);
			cpu.pc = 0x0018;
			cpu.t = 16;
			break;
		}
		case 0xE0:	// LDH (n), A
		{
			unsigned int n = cpu.mem.arr[cpu.pc++];
			writeMemory(0xFF00+n, cpu.a);
			cpu.t = 12;
			break;
		}
		case 0xE1:	// POP HL
		{
			cpu.l = cpu.mem.arr[cpu.sp++];
			cpu.h = cpu.mem.arr[cpu.sp++];
			cpu.t = 12;
			break;
		}
		case 0xE2:	// LDH (C), A
		{
			writeMemory(0xFF00 + cpu.c, cpu.a);
			cpu.t = 8;
			break;
		}
		case 0xE3:	// Non existent op
		{
			break;
		}
		case 0xE4:	// Non existent op
		{
			break;
		}
		case 0xE5:	// PUSH HL
		{
			cpu.mem.arr[--(cpu.sp)] = cpu.h;
			cpu.mem.arr[--(cpu.sp)] = cpu.l;
			cpu.t = 16;
			break;
		}
		case 0xE6:	// AND n
		{
			unsigned int n = cpu.mem.arr[cpu.pc++];
			cpu.a &= n;
			cpu.f = zeroTable[cpu.a] | 0x20;
			cpu.t = 8;
			break;
		}
		case 0xE7:	// RST 20
		{
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc >> 8);
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc);
			cpu.pc = 0x0020;
			cpu.t = 16;
			break;
		}
		case 0xE8:	// ADD SP, d
		{
			int a = (char)(cpu.mem.arr[cpu.pc++]);
			int b = (cpu.sp + a) & 0xFFFF;
			a = cpu.sp ^ a ^ b;
			cpu.f = (((a & 0x100) == 0x100) << 4) |
						(((a & 0x10) == 0x10) << 5);
			cpu.sp = b;
			cpu.t = 16;
			break;
		}
		case 0xE9:	// JP HL
		{
			cpu.pc = cpu.hl;
			cpu.t = 4;
			break;
		}
		case 0xEA:	// LD (nn), A
		{
			unsigned int addr = cpu.mem.arr[cpu.pc+1];
			addr = addr << 8;
			addr |= cpu.mem.arr[cpu.pc];
			writeMemory(addr, cpu.a);
			cpu.pc += 2;
			cpu.t = 16;
			break;
		}
		case 0xEB:	// Non existent op
		{
			break;
		}
		case 0xEC:	// Non existent op
		{
			break;
		}
		case 0xED:	// Non existent op
		{
			break;
		}
		case 0xEE:	// XOR n
		{
			cpu.a ^= cpu.mem.arr[cpu.pc++];
			cpu.f = zeroTable[cpu.a];
			cpu.t = 8;
			break;
		}
		case 0xEF:	// RST 28
		{
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc >> 8);
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc);
			cpu.pc = 0x0028;
			cpu.t = 16;
			break;
		}
		// check here. how does gearboy do input
		case 0xF0:	// LDH A, (n)
		{
			reg8 n = cpu.mem.arr[cpu.pc++];
			if (n == 0)
				cpu.a = ReadJoypad();
			else
				cpu.a = cpu.mem.arr[0xFF00+n];
			cpu.t = 12;
			break;
		}
		case 0xF1:	// POP AF
		{
			// check. f regs low nibble should be zero
			cpu.f = cpu.mem.arr[cpu.sp++] & 0xF0;
			cpu.a = cpu.mem.arr[cpu.sp++];
			cpu.t = 12;
			break;
		}
		case 0xF2:	// LDH A, C
		{
			cpu.a = cpu.mem.arr[0xFF00 + cpu.c];
			cpu.t = 8;
			break;
		}
		// check here. gear boy does DI immediately
		case 0xF3:	// DI
		{
			cpu.IMEOffReq = true;
			cpu.t = 4;
			break;
		}
		case 0xF4:	// Non existent op
		{
			break;
		}
		case 0xF5:	// PUSH AF
		{
			cpu.mem.arr[--(cpu.sp)] = cpu.a;
			cpu.mem.arr[--(cpu.sp)] = cpu.f;
			cpu.t = 16;
			break;
		}
		case 0xF6:	// OR n
		{
			unsigned int n = cpu.mem.arr[cpu.pc++];
			cpu.a |= n;
			cpu.f = zeroTable[cpu.a];
			cpu.t = 8;
			break;
		}
		case 0xF7:	// RST 30
		{
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc >> 8);
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc);
			cpu.pc = 0x0030;
			cpu.t = 16;
			break;
		}
		case 0xF8:	// LDHL SP, d
		{
			int a = (char)(cpu.mem.arr[cpu.pc++]);
			int b = (cpu.sp + a) & 0xFFFF;
			a = cpu.sp ^ a ^ b;
			cpu.f = (((a & 0x100) == 0x100) << 4) |
						(((a & 0x10) == 0x10) << 5);
			cpu.hl = b;
			cpu.t = 12;
			break;
		}
		case 0xF9:	// LD SP, HL
		{
			cpu.sp = cpu.hl;
			cpu.t = 8;
			break;
		}
		case 0xFA:	// LD A, (nn)
		{
			unsigned int addr = cpu.mem.arr[cpu.pc+1];
			addr = addr << 8;
			addr |= cpu.mem.arr[cpu.pc];
			cpu.a = cpu.mem.arr[addr];
			cpu.pc += 2;
			cpu.t = 16;
			break;
		}
		case 0xFB:	// EI
		{
			cpu.IMEOnReq = true;
			cpu.t = 4;
			break;
		}
		case 0xFC:	// Non existent op
		{
			break;
		}
		case 0xFD:	// Non existent op
		{
			break;
		}
		case 0xFE:	// CP n
		{
			int sub = cpu.a - cpu.mem.arr[cpu.pc++];
			cpu.f = zeroTable[sub&0xFF] | 0x40 |
						(((sub & 0xF) > (cpu.a & 0xF))<<5) |
						((sub < 0) << 4);
			cpu.t = 8;
			break;
		}
		case 0xFF:	// RST 38
		{
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc >> 8);
			cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc);
			cpu.pc = 0x0038;
			cpu.t = 16;
			break;
		}
	}

	cpu.T += cpu.t;
}