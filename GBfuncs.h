#ifndef GBFUNCS_H
#define GBFUNCS_H

#define CARTRIDGE_MEM_SZ 0x200000

#include "Banking.h"
#include "Rendering.h"

void writeMemory(unsigned short addr, reg8 value);

////// init functions
void initGPU()
{
	cpu.gpu.GPUcontrol = &(cpu.mem.arr[0xFF40]);
	cpu.gpu.scy = &(cpu.mem.arr[0xFF42]);
	cpu.gpu.scx = &(cpu.mem.arr[0xFF43]);
	cpu.gpu.winy = &(cpu.mem.arr[0xFF4A]);
	cpu.gpu.winx = &(cpu.mem.arr[0xFF4B]);
	cpu.gpu.line = &(cpu.mem.arr[0xFF44]);
	cpu.gpu.scanlineCounter = 0;
}

void initMemory()
{
	cpu.mem.RAMenabled = false;
	cpu.mem.arr[0xFF00] = 0xFF;
	cpu.mem.arr[0xFF05] = 0;
	cpu.mem.arr[0xFF06] = 0;
	cpu.mem.arr[0xFF07] = 0;
	cpu.mem.arr[0xFF10] = 0x80;
	cpu.mem.arr[0xFF11] = 0xBF;
	cpu.mem.arr[0xFF12] = 0xF3;
	cpu.mem.arr[0xFF14] = 0xBF;
	cpu.mem.arr[0xFF16] = 0x3F;
	cpu.mem.arr[0xFF17] = 0;
	cpu.mem.arr[0xFF19] = 0xBF;
	cpu.mem.arr[0xFF1A] = 0x7F;
	cpu.mem.arr[0xFF1B] = 0xFF;
	cpu.mem.arr[0xFF1C] = 0x9F;
	cpu.mem.arr[0xFF1E] = 0xBF;
	cpu.mem.arr[0xFF20] = 0xFF;
	cpu.mem.arr[0xFF21] = 0;
	cpu.mem.arr[0xFF22] = 0;
	cpu.mem.arr[0xFF23] = 0xBF;
	cpu.mem.arr[0xFF24] = 0x77;
	cpu.mem.arr[0xFF25] = 0xF3;
	cpu.mem.arr[0xFF26] = 0xF1;
	cpu.mem.arr[0xFF40] = 0x91;
	cpu.mem.arr[0xFF42] = 0;
	cpu.mem.arr[0xFF43] = 0;
	cpu.mem.arr[0xFF45] = 0;
	cpu.mem.arr[0xFF47] = 0xFC;
	cpu.mem.arr[0xFF48] = 0xFF;
	cpu.mem.arr[0xFF49] = 0xFF;
	cpu.mem.arr[0xFF4A] = 0;
	cpu.mem.arr[0xFF4B] = 0;
	cpu.mem.arr[0xFFFF] = 0;
}

void reset()
{
	// gpu stuff should be set after the loading the rom
	// CPU
	cpu.af = 0x01B0;
	cpu.bc = 0x0013;
	cpu.de = 0x00D8;
	cpu.hl = 0x014D;
	cpu.sp = 0xFFFE;
	cpu.pc = 0x100;
	cpu.T = cpu.t = 0;
	cpu.timerCounter = CLOCKSPEED / 4096;
	cpu.dividerCounter = 0;
	cpu.masterInterrupt = true;
	cpu.IMEOnReq = false;
	cpu.IMEOffReq = false;
	cpu.halted = false;
	cpu.haltBug = false;

	// clear memory. Not necessary
	//memset((void*)(cpu.mem.arr), 0, 0x10000);

	// Cartridge
	if (cpu.cart.RAM != NULL)
	{
		free((void*)cpu.cart.RAM);
		cpu.cart.RAM = NULL;
	}
	memset(cpu.cart.mem, 0, CARTRIDGE_MEM_SZ);
	cpu.cart.ROMBanking = true;
	cpu.cart.currentROMBank = 1;
	cpu.cart.currentRAMBank = 0;
}

int loadROM(const char* filename, int addr)
{
	reset();

	FILE* f;
	f = fopen(filename, "r");
	if (!f) {
		return 1; // rom file not found
	}

	// read into cart memory
	cpu.cart.sz = fread((void*)(cpu.cart.mem), 1, 0x200000, f);
	assert(!ferror(f));
	fclose(f);

	switch (cpu.cart.mem[0x147])
	{
		case 0:
			cpu.cart.mbc = 0;
			break;
		case 1:
		case 2:
		case 3:
			cpu.cart.mbc = 1;
			break;
		case 5:
		case 6:
			cpu.cart.mbc = 2;
			assert(false);
			break;
		default:
			assert(cpu.cart.mem[0x147] == 0);
	}

	// ROM size
	switch (cpu.cart.mem[0x148])
	{
		case 0:	cpu.cart.ROMBankNr = 2; break;
		case 1: cpu.cart.ROMBankNr = 4; break;
		case 2: cpu.cart.ROMBankNr = 8; break;
		case 3: cpu.cart.ROMBankNr = 16; break;
		case 4: cpu.cart.ROMBankNr = 32; break;
		case 5: cpu.cart.ROMBankNr = 64; break;
		case 6: cpu.cart.ROMBankNr = 128; break;
		case 0x52: cpu.cart.ROMBankNr = 72; break;
		case 0x53: cpu.cart.ROMBankNr = 80; break;
		case 0x54: cpu.cart.ROMBankNr = 96; break;
		default: assert(false);
	}

	memcpy(cpu.mem.ROMBank0, cpu.cart.mem, 0x4000);

	cpu.cart.currentROMBank = 1;
	memcpy(cpu.mem.ROMBankN, cpu.cart.mem +
		cpu.cart.currentROMBank * 0x4000, 0x4000);

	// RAM size
	switch (cpu.cart.mem[0x149])
	{
		case 0:
			cpu.cart.RAMBankNr = 0;
			cpu.cart.RAM = NULL;
			break;
		case 1:
			cpu.cart.RAMBankNr = 1;
			cpu.cart.RAM = (reg8*)malloc(2*1024);
			break;
		case 2:
			cpu.cart.RAMBankNr = 1;
			cpu.cart.RAM = (reg8*)malloc(8*1024);
			break;
		case 3:
			cpu.cart.RAMBankNr = 4;
			cpu.cart.RAM = (reg8*)malloc(32*1024);
			break;
		default: assert(false);
	}

	cpu.cart.currentRAMBank = 0;
	if (cpu.cart.RAM)
	{
		memset((void*)cpu.cart.RAM, 0, sizeof(cpu.cart.RAM));
		memcpy(cpu.mem.CartRAM, cpu.cart.RAM +
			cpu.cart.currentRAMBank * 0x2000, 0x2000);
	}

	initMemory();
	initGPU();
	return 0;
}

////// timer functions
void timer()
{
	cpu.dividerCounter += cpu.t;

	// if clock is enabled
	if (cpu.mem.arr[TMC] & 0x4)
	{
		cpu.timerCounter += cpu.t;

		unsigned int freq = 0;
		switch (GetClockFreq())
		{
			case 0: freq = 1024; break;
			case 1: freq = 16; break;
			case 2: freq = 64; break;
			case 3: freq = 256; break;
			default: assert(false);
		}

		while (cpu.timerCounter >= freq)
		{
			cpu.timerCounter -= freq;

			if (cpu.mem.arr[TIMA] == 0xFF)
			{
				RequestInterrupt(2);
				writeMemory(TIMA, cpu.mem.arr[TMA]);
			}
			else
				writeMemory(TIMA, cpu.mem.arr[TIMA]+1);
		}
	}

	if (cpu.dividerCounter >= 256)
	{
		cpu.dividerCounter -= 256;
		cpu.mem.arr[DIV]++;
	}
}

////// interrupts
void serviceInterrupt(int i)
{
	cpu.halted = false;
	cpu.masterInterrupt = false;
	ClearBit(cpu.mem.arr[IF], i);

	// push program counter to stack
	cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc >> 8);
	cpu.mem.arr[--(cpu.sp)] = (reg8)(cpu.pc);

	switch (i)
	{
		case 0: cpu.pc = VBLANK; break;
		case 1: cpu.pc = LCD; break;
		case 2: cpu.pc = TIMER; break;
		case 3: cpu.pc = SERIAL; break;
		case 4: cpu.pc = JOYPAD; break;
		default: assert(false);
	}
}

void handleInterrupts()
{
	// when halted, we check for interrupts even if IME is off
	if (cpu.masterInterrupt || cpu.halted)
	{
		reg8 req = cpu.mem.arr[IF];

		// if there are any interrupt requests
		if (req > 0)
		{
			for (int i = 0; i < 8; ++i)
			{
				bool enabled = cpu.mem.arr[IE];

				// if bit i is requested and enabled
				if ((req & (1 << i)) && (enabled & (1 << i)))
				{
					// if we're halted with IME off, don't serve the interrupt,
					// continue directly to the next op after the halt.
					if (cpu.halted && !cpu.masterInterrupt)
						cpu.halted = false;
					else
						serviceInterrupt(i);
				}
			}
		}
	}
}


reg8 ReadJoypad()
{
	reg8 res = cpu.mem.arr[JPAD];
	res ^= 0xFF;

	if (!(res & (1<<4)))
	{
		reg8 topJpad = jpadState >> 4;
		topJpad |= 0xF0;
		res &= topJpad;
	}
	else if (!(res & (1<<5)))
	{
		reg8 botJpad = jpadState & 0xF;
		botJpad |= 0xF0;
		res &= botJpad;
	}

	return res;
}

void writeMemory(unsigned short addr, reg8 value)
{
	switch (addr & 0xF000)
	{
		// cases when writing to ROM space
		case 0x0000:
		case 0x1000:
		{
			// enable RAM bank writing
			if (cpu.cart.mbc > 0)
				EnableRAMBank(addr, value);
			return;
		}
		case 0x2000:
		case 0x3000:
		{
			// ROM bank change
			if (cpu.cart.mbc > 0)
				ChangeLoROMBank(value);
			return;
		}
		case 0x4000:
		case 0x5000:
		{
			// mode dependent bank change
			if (cpu.cart.mbc == 1)
			{
				if (cpu.cart.ROMBanking)
					ChangeHiROMBank(value);
				else	// change RAM bank
					cpu.cart.currentRAMBank = value & 0x3;
			}
			return;
		}
		case 0x6000:
		case 0x7000:
		{
			// change RAM/ROM mode
			if (cpu.cart.mbc == 1)
				ChangeRAMROMMode(value);
			return;
		}
		case 0xA000:
		case 0xB000:
		{
			// writing to RAM bank
			if (!cpu.mem.RAMenabled)
				return;
			break;
		}
		case 0xC000:
		case 0xD000:
		{
			// echo
			if (addr <= 0xDDFF)
			{
				cpu.mem.arr[addr] = value;
				cpu.mem.arr[addr+0x2000] = value;
				return;
			}
		}
		case 0xE000:
		case 0xF000:
		{
			// echo
			if (addr <= 0xFDFF)
			{
				cpu.mem.arr[addr] = value;
				cpu.mem.arr[addr-0x2000] = value;
				return;
			}

			switch (addr)
			{
				case TMC:
				{
					if (GetClockFreq() != value)
					{
						cpu.mem.arr[addr] = value;
						//SetClockFreq();
					}
					return;
				}
				case DIV:
				{
					// writing to divider register resets it to zero
					cpu.mem.arr[addr] = 0;
					return;
				}
				case 0xFF44:	// scanline addr
				{
					// writing to scanline resets it to zero
					SCANLINE = 0;
					return;
				}
				case DMA:
				{
					// DMA transfer
					unsigned int address = value;
					address <<= 8;
					memcpy(cpu.mem.SpriteInfo, cpu.mem.arr+address, 0xA0);
					return;
				}
				case 0xFF40:	// LCDC
				{
					if (!(LCD_CONTROL&(1<<5)) && (value&(1<<5)))
					{
						// window stuff?
					}
					if (!(LCD_CONTROL & (1<<7)) && (value & (1<<7)))
					{
						cpu.gpu.LCDEnableCounter = 244;
					}
					else if (!(value & (1<<7)))
					{
						cpu.mem.arr[LCD_STATUS] &= 0x7C;
						SCANLINE = 0;
						cpu.gpu.scanlineCounter = 0;
					}
					break;
				}
			}
			break;
		}
	}

	cpu.mem.arr[addr] = value;
}

#include "tables.h"
#include "CBOps.h"
#include "Ops.h"

#endif