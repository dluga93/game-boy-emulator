# Game-Boy-Emulator
Game Boy Emulator in C

Simple Gameboy emulator for windows.
For now, to load a rom, just rename it to "rom.gb" and put it in the same directory as the GB.exe file.

MBC0 (ROM only) games work (ex: tetris)

Working on MBC1.

MBC2 and MBC3 don't work yet.

<h3>Controls:</h3>
<table>
<tr><th>Gameboy</th><th>Keyboard</th></tr>
<tr><td align="center">A</td><td align="center">Z</td></tr>
<tr><td align="center">B</td><td align="center">X</td></tr>
<tr><td align="center">Start</td><td align="center">Enter</td></tr>
<tr><td align="center">Select</td><td align="center">Space</td></tr>
<tr><td align="center">Up</td><td align="center">Up Arrow</td></tr>
<tr><td align="center">Down</td><td align="center">Down Arrow</td></tr>
<tr><td align="center">Left</td><td align="center">Left Arrow</td></tr>
<tr><td align="center">Right</td><td align="center">Right Arrow</td></tr>
</table>
<h3>Other Controls:</h3>
<table>
<tr><th>Key</th><th>Effect</th></tr>
<tr><td align="center">+ (plus key)</td><td align="center">Set zoom to x2</td></tr>
<tr><td align="center">- (minus key)</td><td align="center">Set zoom to x1</td></tr>
</table>
