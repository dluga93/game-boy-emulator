#define _CRT_SECURE_NO_WARNINGS
typedef int bool;
#define true 1
#define false 0

#pragma comment(linker, "/subsystem:windows")

#include <assert.h>
#include <stdio.h>
#include <windows.h>

#define WIDTH 160
#define HEIGHT 144

#include "CPU.h"
#include "hostFuncs.h"
#include "GBfuncs.h"

int CALLBACK WinMain(HINSTANCE instance,
	HINSTANCE prevInstance,
	LPSTR commandLine,
	int showCode)
{
	// create screen buffer
	resizeDIB();

	// create window class
	WNDCLASS WindowClass;
	memset(&WindowClass, 0, sizeof(WindowClass));
	WindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	WindowClass.lpfnWndProc = MainWindowCallback;
	WindowClass.hInstance = instance;
	WindowClass.lpszClassName = "GameBoy";

	// register window class
	if (RegisterClass(&WindowClass))
	{
		RECT clientRect = { 0, 0, 160, 144 };
		AdjustWindowRect(&clientRect, WS_OVERLAPPED | WS_CAPTION |
			WS_SYSMENU | WS_MINIMIZEBOX | WS_VISIBLE, false);

		// create the window
		HWND Window = CreateWindowEx(0,
			WindowClass.lpszClassName, "Game Boy",
			WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU |
			WS_MINIMIZEBOX | WS_VISIBLE,
			CW_USEDEFAULT, CW_USEDEFAULT,
			clientRect.right - clientRect.left,
			clientRect.bottom - clientRect.top,
			0, 0, instance, 0);

		if (Window)
		{
			MSG message;
			deviceContext = GetDC(Window);

			// gameboy code starts
			// try to load rom
			cpu.cart.RAM = NULL;
			if (loadROM("rom.gb", 0) == 1)
			{
				MessageBox(Window, "No 'rom.gb' file found in directory.",
					"ROM file not found", MB_OK|MB_ICONERROR);
				return 0;
			}

			// timing variables
			LARGE_INTEGER PerfCountFrequencyResult;
			QueryPerformanceFrequency(&PerfCountFrequencyResult);
			unsigned long long PerfCountFrequency = PerfCountFrequencyResult.QuadPart;
			LARGE_INTEGER LastCounter, EndCounter;
			QueryPerformanceCounter(&LastCounter);
			double MSperFrame = 0;

			while (GlobalRunning)
			{
				// get and handle OS events/messages
				if (PeekMessage(&message, 0, 0, 0, PM_REMOVE))
				{
					TranslateMessage(&message);
					DispatchMessage(&message);
				}

				// (IME) Interrupt Master Enable handling
				if (cpu.IMEOnReq)
				{
					execNext();
					cpu.IMEOnReq = false;
					cpu.masterInterrupt = true;
				}
				else if (cpu.IMEOffReq)
				{
					execNext();
					cpu.IMEOffReq = false;
					cpu.masterInterrupt = false;
				}
				else
					execNext();

				timer();		// update timer
				step();			// update gpu
				handleInterrupts();

				////// end of gameboy part of main loop
				// put this in the callback when I start putting in menus
				if (zoomChanged)		// handle zooming
				{
					zoomChanged = false;
					GetClientRect(Window, &clientRect);
					int x = clientRect.left;
					int y = clientRect.top;
					clientRect.right = 160 * factor;
					clientRect.bottom = 144 * factor;
					AdjustWindowRect(&clientRect, WS_OVERLAPPED | WS_CAPTION |
						WS_SYSMENU | WS_MINIMIZEBOX | WS_VISIBLE, false);
					MoveWindow(Window, x, y,
						(clientRect.right - clientRect.left),
						(clientRect.bottom - clientRect.top),
						true);
					deviceContext = GetDC(Window);
				}

				if (cpu.T >= CLOCKSPEED / 40)		// setting correct emulator speed
				{
					QueryPerformanceCounter(&EndCounter);
					unsigned long long counter = EndCounter.QuadPart - LastCounter.QuadPart;
					MSperFrame = 1000.0 * ((double)counter / (double)PerfCountFrequency);
					LastCounter = EndCounter;

					if (MSperFrame < 25)
						Sleep(25 - MSperFrame);

					cpu.T -= CLOCKSPEED / 40;
				}
			}
		}
	}

	// free screen buffer
	VirtualFree(cpu.gpu.pixels, 0, MEM_RELEASE);
	return 0;
}