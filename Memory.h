#ifndef MEMORY_H
#define MEMORY_H

typedef unsigned char reg8;

typedef struct Memory
{
	bool RAMenabled;
	union
	{
		reg8 arr[64*1024];		// 64K

		struct
		{
			reg8 ROMBank0[16*1024];	// 16K	0000-3FFF
			reg8 ROMBankN[16*1024];	// 16K	4000-7FFF
			reg8 VRAM[8*1024];		//  8K	8000-9FFF
			reg8 CartRAM[8*1024];	//  8K	A000-BFFF
			reg8 WRAM0[4*1024];		//  4K	C000-CFFF
			reg8 WRAM1[4*1024];		//  4K	D000-DFFF
			reg8 ECHO[7*1024+512];	// 7.5K	E000-FDFF	Same as C000-DDFF
			reg8 SpriteInfo[160];   //      FE00-FE9F   OAM - object attribute memory
			reg8 NotUsable[96];
			reg8 IOPorts[128];
			reg8 HRAM[127];
			reg8 InterruptEnable;
		};
	};
} Memory;

typedef struct Cartridge
{
	reg8 mem[0x200000];
	int sz;
	int mbc;
	reg8 currentROMBank, currentRAMBank;
	reg8 RAMBankNr;
	reg8* RAM;
	reg8 ROMBankNr;
	bool ROMBanking;
} Cartridge;

#endif