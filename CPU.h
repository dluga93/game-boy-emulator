#ifndef CPU_H
#define CPU_H

#define CLOCKSPEED 4194304

// timer addresses
#define DIV 0xFF04	// divider register
#define TIMA 0xFF05	// Timer counter
#define TMA 0xFF06	// Timer modulator
#define TMC 0xFF07	// Timer control

// interrupt addresses
#define IF 0xFF0F	// Interrupt request
#define IE 0xFFFF	// Interrupt enabled
#define VBLANK 0x40	// VBlank interrupt routine
#define LCD 0x48	// LCD interrupt routine
#define TIMER 0x50	// Timer interrupt routine
#define SERIAL 0x58	// Serial interrupt routine
#define JOYPAD 0x60	// Joypad interrupt

// gpu/rendering
#define LCD_CONTROL (*cpu.gpu.GPUcontrol)
#define LCD_STATUS 0xFF41
#define SCANLINE (*cpu.gpu.line)
#define DMA 0xFF46		// write here when launching DMA
#define SCY (*cpu.gpu.scy)	// screen x position in the bg tilemap (in pixels)
#define SCX (*cpu.gpu.scx)	// screen y position in the bg tilemap (in pixels)
#define WINY (*cpu.gpu.winy)	// window y position in the viewing area
#define WINX ((*cpu.gpu.winx)-7)	// window x-7 (+7?) position in the viewing area
#define BG_PALETTE (*(&(cpu.mem.arr[0xFF47])))
#define SPR_PAL1 (*(&(cpu.mem.arr[0xFF48])))
#define SPR_PAL2 (*(&(cpu.mem.arr[0xFF49])))

// joypad
#define JPAD 0xFF00

#define GetClockFreq() (cpu.mem.arr[TMC] & 3)
#define LCDEnabled() ( (LCD_CONTROL & (1<<7)) ? 1 : 0)

#define RequestInterrupt(bitID) SetBit(cpu.mem.arr[IF], bitID)
#define SetBit(value, bit) (value |= (1 << bit))
#define ClearBit(value, bit) value &= ~(1 << bit)

#include "Memory.h"
#include "GPU.h"

typedef struct CPU
{
	union {
		struct {unsigned short af, bc, de, hl;};
		struct {unsigned char f, a, c, b, e, d, l, h;};
	};
	unsigned short pc, sp;
	
	unsigned int T;
	int timerCounter;
	int dividerCounter;
	unsigned int t;
	
	bool masterInterrupt;
	bool IMEOnReq;
	bool IMEOffReq;
	bool halted;
	bool haltBug;

	Memory mem;
	Cartridge cart;
	GPU gpu;
} CPU;

CPU cpu;

#endif