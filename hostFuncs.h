#ifndef HOSTFUNCS_H
#define HOSTFUNCS_H

HDC deviceContext;
bool GlobalRunning = true;
int factor = 1;		// zoom factor
bool zoomChanged = false;
reg8 jpadState = 0xFF;

#define render() StretchDIBits(deviceContext, 0, 0, WIDTH*factor, HEIGHT*factor, 0, 0, WIDTH, HEIGHT, cpu.gpu.pixels, (BITMAPINFO*)(&bmp), DIB_RGB_COLORS, SRCCOPY)

struct MyBitmapInfo
{
	BITMAPINFOHEADER bmiHeader;
	RGBQUAD bmiColors[256];
} bmp;

LRESULT CALLBACK MainWindowCallback(HWND Window,
	UINT Message, WPARAM WParam, LPARAM LParam)
{
	LRESULT Result = 0;

	switch (Message)
	{
		case WM_CLOSE:
		case WM_QUIT:
		{
			GlobalRunning = false;
		}
		break;

		case WM_DESTROY:
		{
			GlobalRunning = false;
		}
		break;

		case WM_SYSKEYDOWN:
		case WM_SYSKEYUP:
		case WM_KEYDOWN:
		case WM_KEYUP:
		{
			unsigned int VKCode = WParam;
			bool WasDown = ((LParam & (1 << 30)) != 0);
			bool IsDown = ((LParam & (1 << 31)) == 0);

			switch (VKCode)
			{
				case VK_OEM_PLUS:
				{
					if (!zoomChanged)
						zoomChanged = true;
					factor = 2;
					break;
				}
				case VK_OEM_MINUS:
				{
					if (!zoomChanged)
						zoomChanged = true;
					factor = 1;
					break;
				}
				case 'Z':
				{
					IsDown ? ClearBit(jpadState, 4) : SetBit(jpadState, 4);
					if (IsDown && !WasDown && !(cpu.mem.arr[JPAD] & (1<<5)))
						RequestInterrupt(4);
					break;
				}
				case 'X':
				{
					IsDown ? ClearBit(jpadState, 5) : SetBit(jpadState, 5);
					if (IsDown && !WasDown && !(cpu.mem.arr[JPAD] & (1<<5)))
						RequestInterrupt(4);
					break;
				}
				case VK_UP:
				{
					IsDown ? ClearBit(jpadState, 2) : SetBit(jpadState, 2);
					if (IsDown && !WasDown && !(cpu.mem.arr[JPAD] & (1<<4)))
						RequestInterrupt(4);
					break;
				}
				case VK_DOWN:
				{
					IsDown ? ClearBit(jpadState, 3) : SetBit(jpadState, 3);
					if (IsDown && !WasDown && !(cpu.mem.arr[JPAD] & (1<<4)))
						RequestInterrupt(4);
					break;
				}
				case VK_LEFT:
				{
					IsDown ? ClearBit(jpadState, 1) : SetBit(jpadState, 1);
					if (IsDown && !WasDown && !(cpu.mem.arr[JPAD] & (1<<4)))
						RequestInterrupt(4);
					break;
				}
				case VK_RIGHT:
				{
					IsDown ? ClearBit(jpadState, 0) : SetBit(jpadState, 0);
					if (IsDown && !WasDown && !(cpu.mem.arr[JPAD] & (1<<4)))
						RequestInterrupt(4);
					break;
				}
				case VK_RETURN:	// start
				{
					IsDown ? ClearBit(jpadState, 7) : SetBit(jpadState, 7);
					if (IsDown && !WasDown && !(cpu.mem.arr[JPAD] & (1<<5)))
						RequestInterrupt(4);
					break;
				}
				case VK_SPACE:	// select
				{
					IsDown ? ClearBit(jpadState, 6) : SetBit(jpadState, 6);
					if (IsDown && !WasDown && !(cpu.mem.arr[JPAD] & (1<<5)))
						RequestInterrupt(4);
					break;
				}
				case VK_F4:
				{
					if (LParam & (1 << 29))	// if alt key was pressed
						GlobalRunning = false;
					break;
				}
			}
		}
		break;

		case WM_PAINT:
		{
			PAINTSTRUCT Paint;
			HDC deviceContext = BeginPaint(Window, &Paint);
			StretchDIBits(deviceContext, 0, 0, WIDTH, HEIGHT, 0, 0,
				WIDTH, HEIGHT, cpu.gpu.pixels, (BITMAPINFO*)(&bmp), DIB_RGB_COLORS, SRCCOPY);
			EndPaint(Window, &Paint);
		}
		break;
		default:
		{
			Result = DefWindowProcA(Window, Message, WParam, LParam);
		}
		break;
	}

	return Result;
}

void resizeDIB()
{
	if (cpu.gpu.pixels) {
		VirtualFree(cpu.gpu.pixels, 0, MEM_RELEASE);
	}

	bmp.bmiHeader.biSize = sizeof(bmp.bmiHeader);
	bmp.bmiHeader.biWidth = WIDTH;
	bmp.bmiHeader.biHeight = -HEIGHT;
	bmp.bmiHeader.biPlanes = 1;
	bmp.bmiHeader.biBitCount = 8;
	bmp.bmiHeader.biCompression = BI_RGB;

	bmp.bmiColors[0].rgbRed = 0xFF;
	bmp.bmiColors[0].rgbBlue = 0xFF;
	bmp.bmiColors[0].rgbGreen = 0xFF;
	bmp.bmiColors[0].rgbReserved = 0;

	bmp.bmiColors[1].rgbRed = 0xCC;
	bmp.bmiColors[1].rgbBlue = 0xCC;
	bmp.bmiColors[1].rgbGreen = 0xCC;
	bmp.bmiColors[1].rgbReserved = 0;

	bmp.bmiColors[2].rgbRed = 0x77;
	bmp.bmiColors[2].rgbBlue = 0x77;
	bmp.bmiColors[2].rgbGreen = 0x77;
	bmp.bmiColors[2].rgbReserved = 0;

	bmp.bmiColors[3].rgbRed = 0;
	bmp.bmiColors[3].rgbBlue = 0;
	bmp.bmiColors[3].rgbGreen = 0;
	bmp.bmiColors[3].rgbReserved = 0;

	int bmpMemSize = WIDTH * HEIGHT;
	cpu.gpu.pixels = (unsigned char*)VirtualAlloc(0, bmpMemSize,
		MEM_RESERVE|MEM_COMMIT,	PAGE_READWRITE);
}

#endif