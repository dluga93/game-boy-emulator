void EnableRAMBank(unsigned short addr, reg8 data)
{
	if (cpu.cart.mbc == 2)
		if (addr & 0x10) // TODO: check if 0x10 is correct value
			return;

	if ((data & 0xF) == 0xA)
		cpu.mem.RAMenabled = true;
	else if ((data & 0xF) == 0)
		cpu.mem.RAMenabled = false;
	else
		assert(0);
}

void ChangeLoROMBank(reg8 data)
{
	if (cpu.cart.mbc == 2)
	{
		cpu.cart.currentROMBank = data & 0xF;
		if (cpu.cart.currentROMBank == 0)
			cpu.cart.currentROMBank++;	// ROMBank 0 is permanently set 0-0x3FFF
		return;
	}

	cpu.cart.currentROMBank &= 0xE0;	// clear the 5 lowest bits
	cpu.cart.currentROMBank |= data & 0x1F;	// set the lowest 5 bits
	if (cpu.cart.currentROMBank == 0)
		cpu.cart.currentROMBank++;

	assert(cpu.cart.currentROMBank < cpu.cart.ROMBankNr);

	memcpy(cpu.mem.ROMBankN, cpu.cart.mem +
		cpu.cart.currentROMBank * 0x4000, 0x4000);
}

void ChangeHiROMBank(reg8 data)
{
	cpu.cart.currentROMBank &= 0x1F;
	data &= 0x60;
	cpu.cart.currentROMBank |= data;
	
	if (cpu.cart.currentROMBank == 0)
		cpu.cart.currentROMBank++;

	assert(cpu.cart.currentROMBank < cpu.cart.ROMBankNr);

	memcpy(cpu.mem.ROMBankN, cpu.cart.mem +
		cpu.cart.currentROMBank * 0x4000, 0x4000);
}

void ChangeRAMROMMode(reg8 data)
{
	data &= 0x1;
	if (data == 0)
	{
		cpu.cart.ROMBanking = true;

		if (cpu.cart.currentRAMBank != 0)
		{
			memcpy(cpu.cart.RAM + cpu.cart.currentRAMBank * 2000,
				cpu.mem.CartRAM, 0x2000);
			cpu.cart.currentRAMBank = 0;
			memcpy(cpu.mem.CartRAM, cpu.cart.RAM +
				cpu.cart.currentRAMBank * 0x2000, 0x2000);
		}
	}
	else
	{
		cpu.cart.ROMBanking = false;
		// is this necessary?
		//cpu.cart.currentROMBank &= 0xE0;
	}
}